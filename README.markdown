`./boydem/` is the [בוידעם][boydem], for things I'll never touch
again.  Another metaphor is a [תל][tel], but it is not as funny.

Mirrors:

* <https://kaka.farm/stagit/dotfiles/log.html>
* <https://codeberg.org/yuvallangerontheroad/dotfiles/>
* <https://git.sr.ht/~kakafarm/dotfiles/>
* <https://bitbucket.org/yuvallanger/dotfiles>
* <https://gitgud.io/yuvallanger/dotfiles>
* <https://github.com/yuvallanger/dotfiles>
* <https://gitlab.com/yuvallanger/dotfiles>

[boydem]: <https://he.wikipedia.org/wiki/%D7%91%D7%95%D7%99%D7%93%D7%A2%D7%9D>
[tel]: <https://he.wikipedia.org/wiki/%D7%AA%D7%9C>
